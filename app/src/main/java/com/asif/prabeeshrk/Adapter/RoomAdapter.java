package com.asif.prabeeshrk.Adapter;

import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.asif.prabeeshrk.Fragment.RoomFragmentAdd;
import com.asif.prabeeshrk.Fragment.RoomFragmentUpdate;
import com.asif.prabeeshrk.Fragment.RoomFragmentUpdateList;
import com.asif.prabeeshrk.R;
import com.asif.prabeeshrk.RoomActivity;
import com.asif.prabeeshrk.RoomDb.User;

import java.util.Collections;
import java.util.List;

public class RoomAdapter extends RecyclerView.Adapter<RoomAdapter.MyViewHolder> {

    private List<User> mUserList;
    private Context mContext;

    public RoomAdapter(List<User> userList, Context context) {
        mUserList = userList;
        mContext = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.list_room_with_action,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {

        User user = mUserList.get(position);
        holder.txtId.setText(String.valueOf(user.getId()));
        holder.txtName.setText(user.getName());
        holder.txtEmail.setText(user.getEmail());

        holder.itemView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                User usr = mUserList.get(holder.getAdapterPosition());
                Bundle bundle = new Bundle();
                bundle.putInt("id", usr.getId());
                Toast.makeText(mContext, usr.getName(), Toast.LENGTH_SHORT).show();
                RoomActivity.sFragmentManager.beginTransaction()
                        .replace(R.id.frgMain, RoomFragmentUpdate.NewInstance(bundle))
                        .addToBackStack("update-form")
                        .commit();
            }
        });

    }

    @Override
    public int getItemCount() {
        return mUserList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView txtId,txtName,txtEmail;
        RelativeLayout viewFront, viewBack;

        public MyViewHolder(View itemView) {
            super(itemView);

            txtId = itemView.findViewById(R.id.txtid);
            txtName = itemView.findViewById(R.id.txtName);
            txtEmail = itemView.findViewById(R.id.txtEmail);

            viewFront = itemView.findViewById(R.id.viewFront);
            viewBack = itemView.findViewById(R.id.viewBack);

        }

    }

    public void removeItem(int position) {
        mUserList.remove(position);
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        notifyItemRemoved(position);
    }

    public void restoreItem(User item, int position) {
        mUserList.add(position, item);
        // notify item added by position
        notifyItemInserted(position);
    }
}
