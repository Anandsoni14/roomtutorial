package com.asif.prabeeshrk.RoomDb;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = User.class, version = 1)
public abstract class MyDb extends RoomDatabase {

    public abstract UserDao userDao();
}
