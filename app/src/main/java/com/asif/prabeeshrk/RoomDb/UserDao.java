package com.asif.prabeeshrk.RoomDb;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface UserDao {

    @Insert
    public void addUser(User user);

    @Query("select * from users")
    public List<User> getUsers();

    @Query("select * from users where id = :id")
    public User getUser(int id);

    @Delete
    public void delUser(User user);

    @Update
    public void updateUser(User user);
}
