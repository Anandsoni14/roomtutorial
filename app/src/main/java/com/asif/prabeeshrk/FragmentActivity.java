package com.asif.prabeeshrk;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.asif.prabeeshrk.Fragment.FragmentOne;
import com.asif.prabeeshrk.Fragment.FragmentTwo;

public class FragmentActivity extends AppCompatActivity implements  View.OnClickListener, FragmentOne.Communicator {

    Button btnLoad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);

        btnLoad = findViewById(R.id.btnLoad);
        btnLoad.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        FragmentOne fragmentOne = new FragmentOne();

        fragmentTransaction.add(R.id.frgContainer, fragmentOne, null)
                .addToBackStack(null)
                .commit();

    }

    @Override
    public void response(String data) {

        Bundle bundle = new Bundle();
        bundle.putString("msg", data);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        FragmentTwo fragmentTwo = new FragmentTwo();
        fragmentTwo.setArguments(bundle);

        fragmentTransaction.replace(R.id.frgContainer,fragmentTwo)
                .addToBackStack(null)
                .commit();

    }
}
