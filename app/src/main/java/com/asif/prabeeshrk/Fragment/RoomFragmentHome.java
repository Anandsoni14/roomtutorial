package com.asif.prabeeshrk.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.asif.prabeeshrk.R;
import com.asif.prabeeshrk.RoomActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class RoomFragmentHome extends Fragment implements View.OnClickListener {

    Button btnAdd, btnView, btnDelete, btnUpdate;

    public RoomFragmentHome() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_room_fragment_home, container, false);

        btnAdd = view.findViewById(R.id.btnAdd);
        btnView = view.findViewById(R.id.btnView);
        btnDelete = view.findViewById(R.id.btnDelete);
        btnUpdate = view.findViewById(R.id.btnUpdate);
        btnAdd.setOnClickListener(this);
        btnView.setOnClickListener(this);
        btnDelete.setOnClickListener(this);
        btnUpdate.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btnAdd:
                RoomActivity.sFragmentManager.beginTransaction().replace(R.id.frgMain, new RoomFragmentAdd()).addToBackStack("add").commit();
                break;
            case R.id.btnView:
                RoomActivity.sFragmentManager.beginTransaction().replace(R.id.frgMain, new RoomFragmentDisplay()).addToBackStack("view").commit();
                break;
            case R.id.btnDelete:
                Toast.makeText(getActivity(), "delete", Toast.LENGTH_SHORT).show();
                RoomActivity.sFragmentManager.beginTransaction().replace(R.id.frgMain, new RoomFragmentDel()).addToBackStack("del").commit();
                break;
            case R.id.btnUpdate:
                Toast.makeText(getActivity(), "delete", Toast.LENGTH_SHORT).show();
                RoomActivity.sFragmentManager.beginTransaction().replace(R.id.frgMain, new RoomFragmentUpdateList()).addToBackStack("update").commit();
                break;

        }

    }
}
