package com.asif.prabeeshrk.Fragment;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.asif.prabeeshrk.Adapter.RecyclerItemTouchHelper;
import com.asif.prabeeshrk.Adapter.RoomAdapter;
import com.asif.prabeeshrk.R;
import com.asif.prabeeshrk.RoomActivity;
import com.asif.prabeeshrk.RoomDb.User;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class RoomFragmentUpdateList extends Fragment implements RecyclerItemTouchHelper.RecyclerItemTouchHelperListener{

    //    TextView txtUpdateList;
    RoomActivity.Communicator mCommunicator;
    RoomAdapter mRoomAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    RecyclerView mRecyclerView;
    List<User> usrList;

    public RoomFragmentUpdateList() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_room_fragment_update_list, container, false);
        usrList = RoomActivity.sMyDb.userDao().getUsers();

        mRoomAdapter = new RoomAdapter(usrList, getActivity());

        mRecyclerView = view.findViewById(R.id.rcvUpdateList);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(mRoomAdapter);
        mRoomAdapter.notifyDataSetChanged();

        // adding item touch helper
        // only ItemTouchHelper.LEFT added to detect Right to Left swipe
        // if you want both Right -> Left and Left -> Right
        // add pass ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT as param
        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(mRecyclerView);

        return view;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, final int position) {

        if (viewHolder instanceof RoomAdapter.MyViewHolder) {
            // get the removed item name to display it in snack bar
            String name = usrList.get(viewHolder.getAdapterPosition()).getName();

            // backup of removed item for undo purpose
            final User deletedItem = usrList.get(viewHolder.getAdapterPosition());
            final int deletedIndex = viewHolder.getAdapterPosition();

            // remove the item from recycler view
            mRoomAdapter.removeItem(viewHolder.getAdapterPosition());

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
            alertDialogBuilder.setTitle("Action").setMessage("Deleted").setNegativeButton("Undo", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    mRoomAdapter.restoreItem(deletedItem, position);
                }
            }).show();
        }
    }
}
