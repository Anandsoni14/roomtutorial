package com.asif.prabeeshrk.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.asif.prabeeshrk.R;
import com.asif.prabeeshrk.RoomActivity;
import com.asif.prabeeshrk.RoomDb.User;


public class RoomFragmentDel extends Fragment {

    Button btnDel;
    EditText edtUserId;

    public RoomFragmentDel() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_room_fragment_del, container, false);

        btnDel = view.findViewById(R.id.btnDel);
        edtUserId = view.findViewById(R.id.edtDelId);

        btnDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = Integer.parseInt(edtUserId.getText().toString());
                User usr = new User();
                usr.setId(id);
                RoomActivity.sMyDb.userDao().delUser(usr);
                Toast.makeText(getActivity(), "User Deleted", Toast.LENGTH_LONG).show();
            }
        });
        return view;
    }

}
