package com.asif.prabeeshrk.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.asif.prabeeshrk.R;
import com.asif.prabeeshrk.RoomActivity;
import com.asif.prabeeshrk.RoomDb.User;

public class RoomFragmentUpdate extends Fragment implements View.OnClickListener {

    EditText edtId, edtName, edtEmail;
    Button btnUpdate;
    static int id;

    User user;

    public RoomFragmentUpdate() {
        // Required empty public constructor
    }
    public static RoomFragmentUpdate NewInstance(Bundle bundle) {
        RoomFragmentUpdate fragmentUpdate = new RoomFragmentUpdate();
        id = bundle.getInt("id");
        return fragmentUpdate;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_room_fragment_update, container, false);

        btnUpdate = view.findViewById(R.id.btnUpdate);
        edtId = view.findViewById(R.id.edtId);
        edtName = view.findViewById(R.id.edtName);
        edtEmail = view.findViewById(R.id.edtEmail);

        user = RoomActivity.sMyDb.userDao().getUser(id);
        edtId.setText(user.getId()+"");
        edtName.setText(user.getName());
        edtEmail.setText(user.getEmail());

        btnUpdate.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {

        User user = new User();

        int id = Integer.parseInt(edtId.getText().toString());
        String name = edtName.getText().toString();
        String email = edtEmail.getText().toString();

        user.setId(id);
        user.setName(name);
        user.setEmail(email);

        RoomActivity.sMyDb.userDao().updateUser(user);

        Toast.makeText(getActivity(), "Record Updated Successfully !", Toast.LENGTH_SHORT).show();

    }
}
