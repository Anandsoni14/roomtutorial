package com.asif.prabeeshrk.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.asif.prabeeshrk.R;
import com.asif.prabeeshrk.RoomActivity;
import com.asif.prabeeshrk.RoomDb.User;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class RoomFragmentDisplay extends Fragment {


    TextView txtDisplayList;

    public RoomFragmentDisplay() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_room_fragment_display, container, false);
        txtDisplayList = view.findViewById(R.id.txtDisplayList);

        List<User> users = RoomActivity.sMyDb.userDao().getUsers();

        String list = "";

        for(User usr:users){

            int id = usr.getId();
            String nm = usr.getName();
            String em = usr.getEmail();
            list = list+"\n\nId : "+id+"\n"+"Name : "+nm+"\n"+"Email : "+em;
        }

        txtDisplayList.setText(list);

        return view;
    }

}
