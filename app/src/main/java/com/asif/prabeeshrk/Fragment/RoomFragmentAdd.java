package com.asif.prabeeshrk.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.asif.prabeeshrk.R;
import com.asif.prabeeshrk.RoomActivity;
import com.asif.prabeeshrk.RoomDb.User;

/**
 * A simple {@link Fragment} subclass.
 */
public class RoomFragmentAdd extends Fragment {

    private EditText  edtId, edtName, edtEmail;
    private Button btnAdd;

    public RoomFragmentAdd() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_room_fragment_add, container, false);

        edtId = view.findViewById(R.id.edtId);
        edtName = view.findViewById(R.id.edtName);
        edtEmail = view.findViewById(R.id.edtEmail);
        btnAdd = view.findViewById(R.id.btnAdd);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int id = Integer.parseInt(edtId.getText().toString());
                String nm = edtName.getText().toString();
                String em = edtEmail.getText().toString();

                User user = new User();
                user.setId(id);
                user.setEmail(em);
                user.setName(nm);
                RoomActivity.sMyDb.userDao().addUser(user);
                Toast.makeText(getActivity(), "User Added Successfully", Toast.LENGTH_SHORT).show();

                edtEmail.setText("");
                edtId.setText("");
                edtName.setText("");
            }
        });

        return view;
    }

}
