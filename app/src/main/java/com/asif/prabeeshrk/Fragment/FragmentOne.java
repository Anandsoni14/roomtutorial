package com.asif.prabeeshrk.Fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.asif.prabeeshrk.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentOne extends Fragment {

    private Button btnLoad;
    private EditText edtMsg;
    private Communicator comm;

    public interface Communicator{

        public void response(String data);
    }

    public FragmentOne() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Activity activity = getActivity();

        try {
            comm = (Communicator) activity;
        }
        catch (ClassCastException e){

            throw new ClassCastException(getActivity().toString()+"class cast error");
        }

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_one, container, false);

         btnLoad = view.findViewById(R.id.btnFrgOne);
         edtMsg = view.findViewById(R.id.edtFrgOne);

         btnLoad.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 String data = edtMsg.getText().toString();
                comm.response(data);

             }
         });


        return  view;
    }

    @Override
    public void onResume() {
        super.onResume();
        edtMsg.setText("");
    }
}
