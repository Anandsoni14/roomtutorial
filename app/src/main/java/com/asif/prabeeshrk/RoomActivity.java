package com.asif.prabeeshrk;

import android.arch.persistence.room.Room;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.asif.prabeeshrk.Fragment.RoomFragmentHome;
import com.asif.prabeeshrk.RoomDb.MyDb;

public class RoomActivity extends AppCompatActivity {

    public static FragmentManager sFragmentManager;
    public static MyDb sMyDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room);

        sMyDb = Room.databaseBuilder(getApplicationContext(),MyDb.class,"userdb").allowMainThreadQueries().build();

        sFragmentManager = getSupportFragmentManager();

        if(findViewById(R.id.frgMain) != null){

            if(savedInstanceState != null){
                return;
            }

            RoomFragmentHome roomFragmentHome = new RoomFragmentHome();

            sFragmentManager.beginTransaction().add(R.id.frgMain, new RoomFragmentHome()).commit();
        }
    }


    public interface Communicator{

        public void sendMsg(int id);

    }
}
